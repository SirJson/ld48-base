﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicPong
{
    class Collision
    {
        public static bool RectangleCollision(Vector2 position1, float width1, float height1, Vector2 position2, float width2, float height2)
        {
            return position1.X < position2.X + width2 &&
                   position2.X < position1.X + width1 &&
                   position1.Y < position2.Y + height2 &&
                   position2.Y < position1.Y + height1;
        }
    }
}

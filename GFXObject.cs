﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicPong
{
    class GFXObject : GameObject
    {
        protected Texture2D tex;

        public GFXObject(Texture2D texture)
        {
            this.tex = texture;
            this.Width = texture.Width;
            this.Height = texture.Height;
        }

        public override void Update()
        {

        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(tex, Position, Color.White);
        }
    }
}

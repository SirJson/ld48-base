﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicPong
{
    class World
    {
        SpriteBatch batch;
        public Dictionary<string, GameObject> Objects = new Dictionary<string, GameObject>();

        public World(SpriteBatch spriteBatch)
        {
            this.batch = spriteBatch;
        }

        public void Draw()
        {
            batch.Begin();
            foreach (var obj in Objects) {
                obj.Value.Draw(batch);
            }
            batch.End();
        }

        public void Update()
        {
            foreach (var obj in Objects)
            {
                obj.Value.Update();
            }
            foreach (var objA in Objects)
            {
                foreach (var objB in Objects)
                {
                    if (objA.Key == objB.Key)
                        continue;

                    if (Collision.RectangleCollision(objA.Value.Position, objA.Value.Width, objA.Value.Height, objB.Value.Position, objB.Value.Width, objB.Value.Height))
                        objA.Value.OnCollide(objB.Value);
                }
            }
        }
    }
}

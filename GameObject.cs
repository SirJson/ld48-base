﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicPong
{
    abstract class GameObject
    {
        public Vector2 Position;
        public float Width { get; protected set; }
        public float Height { get; protected set; }
        public abstract void Draw(SpriteBatch batch);
        public abstract void Update();
        public virtual void OnCollide(GameObject other) { }
    }
}
